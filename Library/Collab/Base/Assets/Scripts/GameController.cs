﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
  
    public GameObject girasol;
    public GameObject nuez;
    public GameObject lanzaGisantes;
    public GameObject lanzaGisantesDoble;
    public GameObject lanzaGisantesHielo;
    public GameObject mina;


    private Vector2 nuezpos = new Vector2(-3.86f, -2.9f);
    private Vector2 lanzaGisantesPos = new Vector2(-2.66f, -2.9f);
    private Vector2 lanzaGisantesDosPos = new Vector2(-1.46f, -2.9f);
    private Vector2 lanzaGisantesHieloPos = new Vector2(-0.25f, -2.9f);
    private Vector2 minaPos = new Vector2(0.85f, -2.83f);

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void spawnGirasol()
    {
        GameObject newGirasol = Instantiate(girasol);
        newGirasol.transform.position = new Vector2(-5.1f, -2.9f);
    }   

    public void spwanNuez()
    {
        GameObject newNuez = Instantiate(nuez);
        newNuez.transform.position = nuezpos;
    }

    public void spawnLanzaGisantes()
    {
        GameObject newLanzaGisantes = Instantiate(lanzaGisantes);
        newLanzaGisantes.transform.position = lanzaGisantesPos;

    }

    public void spawnLanzaGisantesDoble()
    {
        GameObject newLanzaGisantesDoble = Instantiate(lanzaGisantesDoble);
        newLanzaGisantesDoble.transform.position = lanzaGisantesDosPos;
    }

    public void spawnLanzaGisantesHielo()
    {
        GameObject newLanzaGisantesHielo = Instantiate(lanzaGisantesHielo);
        newLanzaGisantesHielo.transform.position = lanzaGisantesHieloPos;
    }

    public void spawnMina()
    {
        GameObject newMina = Instantiate(mina);
        newMina.transform.position = minaPos;
    }


}
