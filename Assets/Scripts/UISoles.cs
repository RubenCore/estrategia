﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class UISoles : MonoBehaviour
{
    GameObject gameCont;

    // Start is called before the first frame update
    void Start()
    {
        
        //subscripcio a la funcio que actualitza el marcador de soles
        
        GameController.instance.ActualitzarUIEvent += actualitzarUI;

    }
    /**
    * funcion para escribir el marcador de soles a traves de los puntos contados en el GameController
    */
    void actualitzarUI()
    {
        int a = GameController.Soles;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = ""+ a;
    }

}
