﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    public float speed = 6f;


    void Start()
    {        
        GameObject.Destroy(this.gameObject, 1f);
    }

    // le da el movimiento al disparo
    void Update()
    {
       
        this.transform.Translate(Vector2.right * speed * Time.deltaTime);
    }
       
}
