﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    private float vel = -0.5f ;
    public int vida = 5;
    private bool frozen = false;
    public int dmgZombie = 2;

    public bool enCoolDown = false;
    public float coolDownAtk = 1f;
    public bool quitandoCoolDown = false;

    public Transform posFinal;
    /*
     * direcion del movimiento y la velocidad a la que se muege , ademas si llega a la zona donde estan los cortacesped
     * y no hay ningun cortacesped desaparece.
     */
    void Update()
    {
        this.transform.Translate(new Vector2(vel * Time.deltaTime, 0));
        if (!frozen)
        {
            this.vel = -0.5f;
        }

        if (Vector2.Distance(posFinal.position, this.transform.position) < 0.2f) {
          Destroy(this.gameObject);
        }
    }

    /*
     * Mira todas las colisiones
     */
    private void OnCollisionEnter2D(Collision2D collision)
    {
        /*
        * si recibe un discparo normal le quita 1 vida 
        */
        if (collision.gameObject.tag == "Disparo")
        {
            GameObject.Destroy(collision.gameObject);
            mirarVida(1);
        }
        /*
        * si recibe un discparo azul le quita 1 vida y lo detiene hasta el siguiente impacto
        */
        if (collision.gameObject.tag == "DisparoAzul")
        {
            if (frozen)
            {
                GameObject.Destroy(collision.gameObject);
                this.frozen = false;
            }else
            {
                GameObject.Destroy(collision.gameObject);
                this.vel = 0f;
                this.frozen = true;
            }
            mirarVida(1);
        }

        /*
        * si colisiona con uno de los cortacespeds el zombie muere
        */
        if (collision.gameObject.tag == "cortacesped1" || collision.gameObject.tag == "cortacesped2" 
            || collision.gameObject.tag == "cortacesped3" || collision.gameObject.tag == "cortacesped4" 
            || collision.gameObject.tag == "cortacesped5" || collision.gameObject.tag == "cortacesped6")
        {
           Destroy(this.gameObject);
        }

        /*
         * si colisiona con la mina hace insta kill al zombie
         */
        if (collision.gameObject.tag == "mina")
        {
            mina m = collision.gameObject.GetComponent<mina>();
            if (m.vivo)
            {
                GameObject.Destroy(collision.gameObject);
                mirarVida(m.dmg);
            }
        }
    }
    /*
     * mira la vida y se le pasa el daño de la mina cuando se mira la colision con la mina
     */
    void mirarVida(int daño)
    {
        vida -= daño;
        if(vida <= 0)
        {
           Destroy(this.gameObject);
        }
    }


    //corrutina
    //getdmg devuelve daño y tambien empieza una corrutina 
    //que cambiara el daño a 0 hasta que pase el coolDown
    public int GetDmg()
    {
        bool siDevolver = false;       
        if (!enCoolDown && !quitandoCoolDown)
        {
            siDevolver = true;            
            enCoolDown = true;
            StartCoroutine(CoolDownAtk());
        }

        if (!siDevolver)
        {            
            return 0;
        }
        else
        {           
            return dmgZombie;
        }
        
    }
    
    IEnumerator CoolDownAtk()
    {        
        quitandoCoolDown = true;
        //theadsleep de java        
        yield return new WaitForSeconds(coolDownAtk);
        enCoolDown = false;
        quitandoCoolDown = false;
    }

}

