﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sol : MonoBehaviour
{
    GameObject gameCont;

    // Start is called before the first frame update
    void Start()
    {
        gameCont = GameObject.Find("GameController");
    }
      
    /**
     * Funcion que elimina los soles cuando los clickamos 
     * y suma +5 soles al marcador de soles 
     */
    private void OnMouseDown()
    {
        GameObject.Destroy(this.gameObject);
        GameController.Soles += 5;
    }
}
