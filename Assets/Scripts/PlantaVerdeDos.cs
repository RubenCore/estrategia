﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantaVerdeDos : MonoBehaviour
{
    private Vector3 offsetMouse = new Vector3(.7f, -.3f, 0);
    private Vector3 offsetGridCell = new Vector3(-.5f, .2f, 0);
    bool isDragging = false;
    float duration = 4;
    bool puedeDispara = false;
    private float t = 0;
    public GameObject bala;
    private Vector2 position2;

    public int vidaNum = 4;
    public bool vivo = false;
    
    void Update()
    {

        if (puedeDispara) {
            if (t >= .9)
            {
                Disparar();
                t = 0;
            }
            if (t < 1)
            {
                t += Time.deltaTime / duration;
            }
        }      
    }
    /**
     * Funcion que instancia dos disparos
     */
    private void Disparar()
    {
        GameObject aux = (GameObject)Instantiate(bala);
        aux.transform.position = this.transform.position;
        position2 = new Vector2(this.transform.position.x+1, this.transform.position.y);
        GameObject aux2= (GameObject)Instantiate(bala);
        aux2.transform.position = position2;
        Invoke("ponerVivo", 0f);
    }

    /**
     * cuando suelta la planta , mira si se puede draggear , entonces coge la posicion de la celda en funcion del grid, 
     * tambien coge todos los coliders y mira si se encuentra una mina , zombie , cortacesped  o cualquier tipo de planta si encuentra uno de esos coliders 
     * lo pone en la posicion de spawn
     */
    private void OnMouseUp()
    {
        if (isDragging)
        {
            isDragging = false;
            Grid grid = FindObjectOfType<Grid>();
            Vector3Int cellPosition = grid.WorldToCell(transform.position + offsetMouse);
            Vector3 posicion = grid.GetCellCenterWorld(cellPosition) + offsetGridCell;
            //coje todas las colisiones en un circulo donde se coloca la planta con un rango de x
            //si se encuentra una mina/zombie o cualquier tipo de planta no puede consturir
            Collider2D[] hitColls = Physics2D.OverlapCircleAll(posicion, .25f);
            foreach (Collider2D i in hitColls)
            {
                if ((i.transform.CompareTag("Player") || i.transform.CompareTag("mina") ||
                    i.transform.CompareTag("zombie") || i.transform.CompareTag("cortacesped1") ||
                    i.transform.CompareTag("cortacesped2") || i.transform.CompareTag("cortacesped3") ||
                    i.transform.CompareTag("cortacesped4") || i.transform.CompareTag("cortacesped5") ||
                    i.transform.CompareTag("cortacesped6")) && i != GetComponent<Collider2D>())
                {
                    transform.position = GameController.instance.lanzaGisantesDosPos;
                    return;
                }
            }
            transform.position = posicion;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            puedeDispara = true;

        }
    }
     /**
     *  permite mover la planta con el raton 
     */
    private void OnMouseDrag()
    {
        isDragging = true;
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 posicionTemporal = new Vector3(pos.x, pos.y + .25f, 0);
        this.transform.position = posicionTemporal;

    }

    /**
    *  mira la colision de la planta con el zombie , si colisiona con el zombie le pasa el daño a la funcion de mirar vida 
    *  
    *  mirar vida hace el calculo de de vida y si llega a 0 o menor elimina la planta
    */
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "zombie")
        {
            Zombie z = collision.gameObject.GetComponent<Zombie>();
            if (this.vivo)
            {
                mirarVida(z.GetDmg());
            }
        }
    }
    void mirarVida(int daño)
    {
        vidaNum -= daño;
        if (vidaNum <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void ponerVivo()
    {
        vivo = true;
    }

}
