﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPuntuacion : MonoBehaviour
{
    GameObject gc;

    // Start is called before the first frame update
    void Start()
    {

        
        //subscripcio a la funcio que actualitza el marcador de boosts
        GameController.instance.ActualitzarUIEvent += actualitzarPuntuacion;
        
    }

    /**
     * funcion para escribir el marcador de boosts a traves de los puntos contados en el GameController
     */
    private void actualitzarPuntuacion()
    {
        int cont = GameController.puntuacion;
        this.GetComponent<TMPro.TextMeshProUGUI>().text=""+cont;
    }

 }
