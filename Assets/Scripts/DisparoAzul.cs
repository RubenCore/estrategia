﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoAzul : MonoBehaviour
{
    public float speed = 6f;

    // Start is called before the first frame update
    void Start()
    {
        GameObject.Destroy(this.gameObject, 1f);
    }

    // le da el movimiento al disparo
    void Update()
    {
        this.transform.Translate(Vector2.right * speed * Time.deltaTime);
    }
}
