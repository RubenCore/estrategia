﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteScript : MonoBehaviour
{
    // Start is called before the first frame update             

    public Sprite b1;
    public Sprite b2;

    public Button buttonChange;
    public int buttonValue = 1;
    void Start()
    {
        buttonChange.image.sprite = b1;
    }

    // Update is called once per frame
    public void ChangeImage()
    {

        buttonValue++;

        switch (buttonValue)
        {
            case 1:
                buttonChange.image.sprite = b2;
                buttonValue = 1;
                AudioListener.volume = 0;
                break;

            case 2:
                buttonChange.image.sprite = b1;
                buttonValue = 0;
                AudioListener.volume = 1;
                break;
            default:
                break;
        }

    }
}
