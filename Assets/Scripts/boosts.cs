﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boosts : MonoBehaviour
{

    public PlantaVerdeNormal p;
    GameObject gc;
    // Start is called before the first frame update
    void Start()
    {
        
    }    

    /**
     * Cuando clickas sobre un boost aumenta el contador de puntuacion
     * y destruye el boost
     */
    private void OnMouseDown()
    {
        GameController.puntuacion += 1;
        Destroy(this.gameObject);        
    }
    
}
