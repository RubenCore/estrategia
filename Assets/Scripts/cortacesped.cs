﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cortacesped : MonoBehaviour
{
    bool correr = false;
    static int restCortacesped;
    private void Start()
    {
        //contador de cortacespeds restantes in game
        restCortacesped = 6;
    }
    // Update is called once per frame
    void Update()
    {
        /**
         * si se activa correr este recorre toda su filera eliminando a todos los enemigos en la fila 
         */
        if (correr)
        {          
            this.transform.Translate(new Vector2(0.05f, 0));
            if(this.transform.position.x >= 6.8f)
            {
                Destroy(this.gameObject);
                restCortacesped--;
            }
        }

        //Si no quedan cortacespeds pierdes
        if(restCortacesped <= 0)
        {
            GameController.instance.swapScene(1);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {    /*
          * si el zombie colisiona con el cortacesped se activa un boolean que activa el movimiento del cortacesped
          */
        if (collision.gameObject.tag == "zombie")
        {
            correr = true;
            //&= ~ mascara inversa de negacion
            GetComponent<Rigidbody2D>().constraints &= ~RigidbodyConstraints2D.FreezePositionX;
        }

    }
    
    
}
