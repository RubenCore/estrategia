﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour
{
    //vuelve a la escena del nivel 
    public void swapScene(int scene)
    {
        SceneManager.LoadScene(3);        
    }
}
