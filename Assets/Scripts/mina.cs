﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mina : MonoBehaviour
{
    private Vector3 offsetMouse = new Vector3(.7f, -.3f, 0);
    private Vector3 offsetGridCell = new Vector3(-.5f, .2f, 0);
    bool isDragging = false;    
    public bool vivo = false;
    private float cont = 3f;
    public int dmg = 5;


    /**
     * cuando suelta la planta , mira si se puede draggear , entonces coge la posicion de la celda en funcion del grid, 
     * tambien coge todos los coliders y mira si se encuentra una mina , zombie , cortacesped  o cualquier tipo de planta si encuentra uno de esos coliders 
     * lo pone en la posicion de spawn
     */
    private void OnMouseUp()
    {
        if (isDragging)
        {
            isDragging = false;
            Grid grid = FindObjectOfType<Grid>();
            Vector3Int cellPosition = grid.WorldToCell(transform.position + offsetMouse);
            Vector3 posicion = grid.GetCellCenterWorld(cellPosition) + offsetGridCell;
            //coje todas las colisiones en un circulo donde se coloca la planta con un rango de x
            //si se encuentra una mina/zombie o cualquier tipo de planta no puede consturir
            Collider2D[] hitColls = Physics2D.OverlapCircleAll(posicion, .25f);
            foreach (Collider2D i in hitColls)
            {
                if ((i.transform.CompareTag("Player") || i.transform.CompareTag("mina") ||
                    i.transform.CompareTag("zombie") || i.transform.CompareTag("cortacesped1") ||
                    i.transform.CompareTag("cortacesped2") || i.transform.CompareTag("cortacesped3") ||
                    i.transform.CompareTag("cortacesped4") || i.transform.CompareTag("cortacesped5") ||
                    i.transform.CompareTag("cortacesped6")) && i != GetComponent<Collider2D>())
                {
                    transform.position = GameController.instance.minaPos;
                    return;
                }
            }
            transform.position = posicion;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            Invoke("ponerVivo", cont);

        }

    }

    /**
   *  permite mover la planta con el raton 
   */
    private void OnMouseDrag()
    {
        isDragging = true;
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 posicionTemporal = new Vector3(pos.x, pos.y + .25f, 0);
        this.transform.position = posicionTemporal;

    }
    
    //mirar con sistema de eventos
    private void ponerVivo()
    {
            vivo = true;
    }
}
