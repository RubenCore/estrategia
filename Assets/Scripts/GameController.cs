﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    public GameObject girasol;
    public GameObject nuez;
    public GameObject lanzaGisantes;
    public GameObject lanzaGisantesDoble;
    public GameObject lanzaGisantesHielo;
    public GameObject mina;
    public GameObject sol;
    public GameObject zombie;

    public int cantCortacespeds = 6;

    public delegate void UISoles();
    public event UISoles ActualitzarUIEvent;

    public delegate void UITemps(float temps);
    public event UITemps ActualitzarUITemps;
    public static float temps ;

    public static int Soles;

    public Transform[] spawnPos;
    public Transform[] finCaminoZombe;

    public GameObject boost;
    public delegate void UIPuntuacion();
    public event UIPuntuacion ActualitzarUIEvent2;
    public static int puntuacion;

    public Vector2 girasolPos = new Vector2(-2.8f, -2.8f);
    public Vector2 nuezPos = new Vector2(-1.55f, -2.8f);
    public Vector2 lanzaGisantesPos = new Vector2(-0.45f, -2.8f);
    public Vector2 lanzaGisantesDosPos = new Vector2(0.7f, -2.8f);
    public Vector2 lanzaGisantesHieloPos = new Vector2(2f, -2.8f);
    public Vector2 minaPos = new Vector2(3f, -2.83f);

    public float tiempoDeSpawn = 6f;

    // hacemos el GameController single tone
    // hacemos un invoke de la corrutina de spawn de zombies
    // hacemos un invokeRepiting de los boost
    void Start()
    {

       
        //singleton
        if (instance == null)
        {
            instance = this;           
        }
        else
        {
            Destroy(this.gameObject);
        }
        Soles = 100;
        puntuacion = 0;
        temps = 0;

        Invoke("llamarCorrutinaSpawn", 1.5f);
        InvokeRepeating("spawnboost", 10f, 50f);
    }

    public void llamarCorrutinaSpawn()
    {
        StartCoroutine(spawnZombie());
    }

    
    void Update()
    {
        if(Soles != -1)
        {
            if (ActualitzarUIEvent != null)
            {
                ActualitzarUIEvent.Invoke();
            }
        }

        if (puntuacion != -1)
        {
            if (ActualitzarUIEvent2 != null)
            {
                ActualitzarUIEvent2.Invoke();
            }
        }
        temps += Time.deltaTime;

        if (ActualitzarUITemps != null)
        {
            ActualitzarUITemps.Invoke(temps);
            if(temps >= 90){
                swapScene(2);
            }
        }



    }
    //spawnea los boost
    public void spawnboost()
    {
        GameObject insboost = Instantiate(boost);
        float random = Random.Range(-8f, 7.5f);
        insboost.transform.position = new Vector2(random, 4.25f);
    }

    //corrutina de los spawns de zombies para que vaya incrementando el tiempo del spawn
    IEnumerator spawnZombie()
    {
        GameObject newZombie = Instantiate(zombie);
        int i = Random.Range(0, spawnPos.Length);
        newZombie.transform.position = spawnPos[i].position;
        newZombie.GetComponent<Zombie>().posFinal = finCaminoZombe[i%6];
        yield return new WaitForSeconds(tiempoDeSpawn);
        tiempoDeSpawn -= 0.2f;
        //llamo a la corrutina despues de haber disminuido el tiempo de spawn 
        StartCoroutine(spawnZombie());
    }

    public void swapScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    //spawneo de girasoles que esta vinculado al boton de la interfaz 
    public void spawnGirasol()
    {
        
        if (Soles>= 10)
        {
            Soles -= 10;
            GameObject newGirasol = Instantiate(girasol);
            newGirasol.transform.position = girasolPos;
        }
    }

    //spawneo de nuez que esta vinculado al boton de la interfaz 
    public void spwanNuez()
    {
        if (Soles >= 10)
        {
            Soles -= 10;
            GameObject newNuez = Instantiate(nuez);
            newNuez.transform.position = nuezPos;
        }
    }

    //spawneo de lanza gisantes normal que esta vinculado al boton de la interfaz 
    public void spawnLanzaGisantes()
    {
        if (Soles >= 10)
        {
            Soles -= 10;
            GameObject newLanzaGisantes = Instantiate(lanzaGisantes);
            newLanzaGisantes.transform.position = lanzaGisantesPos;
        }
    }

    //spawneo de lanza gisantes doble que esta vinculado al boton de la interfaz 
    public void spawnLanzaGisantesDoble()
    {
        if (Soles >= 10)
        {
            Soles -= 10;
            GameObject newLanzaGisantesDoble = Instantiate(lanzaGisantesDoble);
            newLanzaGisantesDoble.transform.position = lanzaGisantesDosPos;
        }
    }

    //spawneo de lanza gisantes de hielo que esta vinculado al boton de la interfaz 
    public void spawnLanzaGisantesHielo()
    {
        if (Soles >= 10)
        {
            Soles -= 10;
            GameObject newLanzaGisantesHielo = Instantiate(lanzaGisantesHielo);
            newLanzaGisantesHielo.transform.position = lanzaGisantesHieloPos;
        }
    }

    //spawneo de mina que esta vinculado al boton de la interfaz 
    public void spawnMina()
    {
        if (Soles >= 10)
        {
            Soles -= 10;
            GameObject newMina = Instantiate(mina);
            newMina.transform.position = minaPos;
        }
    }

    


}
