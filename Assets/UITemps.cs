﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITemps : MonoBehaviour
{
    GameObject gc;
    void Start()
    {
        gc = GameObject.Find("GameController");
        gc.GetComponent<GameController>().ActualitzarUITemps += actualitzarUI;

    }

    private void actualitzarUI(float temps)
    {
        int tiempo = (int)temps;
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Tiempo : " + tiempo;
    }
}
