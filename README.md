**Control por raton**

    Cada planta cuesta 10 soles , comienzas con 100 soles. 
    Cuando sale un boost tienes que comprar una planta con corra que es la normal y  ponerla en el tablero.
    Si aguantas los 90s ganas si, si te quedas sin cortacespeds pierdes.

**Requisitos minimos**  
    
    Control per mouse. Clicks, Drags, etc.
    Mapa creat mitjançant tilemap, isomètric.
    UI complexa amb diferents elements interactuables.
    Gestió de recursos(soles , boost).
    Diverses escenes, amb persistència de dades entre les escenes
    Pas d’informació entre Scripts mitjançant delegats i un sistema d’events
    Informació per pantalla mitjançant canvas. Ús d’imatges i botons
    Dificultat dinàmica i incremental.
   

**Requisitos adicionales**   

    Escenes extres
    Efectes visuals extres, com so      
    Accés a components per codi més enllà del RigidBody i el Transform
    Corrutinas para controlar el daño del zombie y el spawn de zombies
    Modificacion del grid obteniendo la celda para posicionar la planta en el centro de la celda    

  

